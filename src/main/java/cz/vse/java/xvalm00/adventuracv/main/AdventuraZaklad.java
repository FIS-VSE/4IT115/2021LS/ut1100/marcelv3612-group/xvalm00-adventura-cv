package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class AdventuraZaklad extends Application {


    private IHra hra = Hra.getSingleton();
    private TextField uzivateluvVstup;
    private Label zadejPrikazLabel;
    private HBox spodniBox;
    private TextArea konzole;
    private BorderPane hlavniBorderPane = new BorderPane();
    private VBox hlavniVBox = new VBox();
    private MenuBar menuBar;
    private HerniPlocha herniPlocha;
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = Hra.getSingleton();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else if (args[0].equals("-gui")) {
                launch(args);
            }
            else {
                System.out.println("Byl zadan neplatny parametr.");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) {

        pripravMenu();

        herniPlocha = new HerniPlocha(hra.getHerniPlan());
        AnchorPane planekHry = herniPlocha.getAnchorPane();

        panelVychodu = new PanelVychodu(hra.getHerniPlan());
        ListView<String> listView = panelVychodu.getListView();

        panelBatohu = new PanelBatohu(hra);

        pripravKonzoli();
        pripravTextField(konzole);
        pripravSpodniPanel(zadejPrikazLabel);

        pripravBorderPane(planekHry, listView, panelBatohu);

        pripravScenuAStage(primaryStage);

        hlavniVBox.getChildren().addAll(menuBar, hlavniBorderPane);

        uzivateluvVstup.requestFocus();
    }








    private void pripravMenu() {
        Menu souborMenu = new Menu("Soubor");

        // nova hra
        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));
        MenuItem novaHra = new MenuItem("Nova hra", novaHraIkonka);
        novaHra.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        novaHra.setOnAction(event -> {
            hra = Hra.restartHry();
            konzole.setText(hra.vratUvitani());
            hra.getHerniPlan().registerObserver(herniPlocha);
            hra.getHerniPlan().registerObserver(panelVychodu);
            hra.getBatoh().registerObserver(panelBatohu);
            herniPlocha.novaHra(hra.getHerniPlan());
            panelVychodu.novaHra(hra.getHerniPlan());
            panelBatohu.novaHra(hra.getBatoh());
            uzivateluvVstup.requestFocus();
        });

        // oddelovac
        SeparatorMenuItem separator = new SeparatorMenuItem();

        // konec
        MenuItem konec = new MenuItem("Konec");
        konec.setOnAction(event -> System.exit(0));

        souborMenu.getItems().addAll(novaHra, separator, konec);

        Menu napovedaMenu = new Menu("Nápověda");
        MenuItem oAplikaci = new MenuItem("O aplikaci");
        MenuItem napovedaHtm = new MenuItem("Nápověda k aplikaci");
        napovedaMenu.getItems().addAll(oAplikaci, napovedaHtm);

        oAplikaci.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Graficka adventura");
            alert.setHeaderText("JavaFX adventura");
            alert.setContentText("verze LS 2020");
            alert.showAndWait();
        });

        napovedaHtm.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource("/zdroje/napoveda.htm").toExternalForm());
            stage.setScene(new Scene(webView, 500, 500));
            stage.show();
        });

        menuBar = new MenuBar();

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);


    }











    private void pripravScenuAStage(Stage primaryStage) {
        Scene scene = new Scene(hlavniVBox, 600, 450);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Adventura");
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    private void pripravBorderPane(AnchorPane planekHry, ListView<String> listView, PanelBatohu panelBatohu) {
        hlavniBorderPane.setTop(planekHry);
        hlavniBorderPane.setRight(listView);
        hlavniBorderPane.setCenter(konzole);
        hlavniBorderPane.setLeft(panelBatohu.getPanel());
        hlavniBorderPane.setBottom(spodniBox);
    }

    private void pripravSpodniPanel(Label zadejPrikazLabel) {
        spodniBox = new HBox();
        spodniBox.setAlignment(Pos.CENTER);
        spodniBox.getChildren().addAll(zadejPrikazLabel, uzivateluvVstup);
    }

    private void pripravTextField(TextArea konzole) {
        uzivateluvVstup = new TextField();
        uzivateluvVstup.setOnAction(event -> {
            String prikaz = uzivateluvVstup.getText();
            konzole.appendText("\n" + prikaz + "\n");
            uzivateluvVstup.setText("");
            String odpovedHry = hra.zpracujPrikaz(prikaz);
            konzole.appendText("\n" + odpovedHry + "\n");

            if (hra.konecHry()) {
                uzivateluvVstup.setEditable(false);
            }
        });

        zadejPrikazLabel = new Label("Zadej příkaz: ");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
    }

    private void pripravKonzoli() {
        konzole = new TextArea();
        konzole.setText(hra.vratUvitani());
        konzole.setEditable(false);
    }
}
